-module(player).
-compile([export_all]).


start_to_play(Player_name)->
    PID = spawn(player, play_loop, [Player_name]),
    PID.

play_loop(Player_name)->
    receive
	{Sender, Ball} ->
	    Sender ! play_ball(Player_name, Ball),
	    player:play_loop(Player_name)
    end.

play_ball(Player_name, Ball)->
    Shot = case Ball of
	       "right" ->
		   "forehand!";
	       "left" ->
		   "oops"
	   end,
    lists:flatten(io_lib:fwrite("~15s : ~s", [Player_name,  Shot])).




















demo_isolation_multiple_players()->
    Player_names = ["Eric", "Marion",
		    "Maurice", "Margot",
		    "Joop", "Mireille",
		    "Jaap", "Helen",
		    "Jan", "Antigona"],
    Players = [start_to_play(Player) || Player <- Player_names ],
    train_players(Players).

train_players([])->
    io:fwrite("No more player!~n"),
    done;

train_players(Players)->
    New_players =
	lists:filter(
	  fun(Player)->
		  Ball = random_shot(7,["left", "smash", "right"]),
		  Player !  {self(), Ball},
		  receive Shot -> io:fwrite("~s~n", [Shot]), true
		  after 10  -> false
		  end
	  end, Players),
    timer:sleep(400),
    player:train_players(New_players).














demo_upgrade_eric()->
    spawn(player, train_loop, [start_to_play("Eric"), 1000]).

train_loop(Player, Sleeptime)->
    Ball = random_shot(3, ["left","right"]),
    io:fwrite("Ball on the ~5s... ", [Ball]),
    Player !  {self(), Ball},
    receive Shot -> io:fwrite("~s~n", [Shot])
    after 2000  -> exit ("The player did not send back the ball")
    end,
    timer:sleep(Sleeptime),
    player:train_loop(Player, Sleeptime).






















play_sound_ok()->
    os:cmd ("/usr/bin/play -q presentation/son/balle.flac").

play_sound_missed()->
    os:cmd ("/usr/bin/play -q presentation/son/missed.flac").

random_shot()->
    random_shot(2).
random_shot(N)->
    random_shot(N, ["left", "right"]).

random_shot(N, Shots)->
    Nth = lists:min([random:uniform(N), length(Shots)]),
    lists:nth(Nth, Shots).





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

spawn_recall()->
    spawn(player, recall_before_thelast_one, [undefined]).

recall_before_thelast_one(Last_one)->
    receive "chicken" -> exit(arggghhh);
	    New_message -> io:fwrite("Last message  : ~s~n", [Last_one]),
			   recall_before_thelast_one(New_message)
    end.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

play_and_respond()->
    receive
	{Sender, "right"} ->   Sender ! "forehand", play_and_respond();
	{Sender, "left"} ->    Sender ! "backhand", play_and_respond();
	{Sender, "chicken"} -> Sender ! "i'm dead"
    end.




%% erl +P1000000
demo_players(Nb_of_players, Nb_of_times)->
    T0 = unixmtime(),
    Start_player = fun () -> spawn(player, play_and_respond, []) end,
    Players = repeat(Start_player, Nb_of_players),
    Launch_balls = fun()-> launch_balls(Players) end,
    repeat(Launch_balls, Nb_of_times),
    shoot_them_all(Players),
    Result = start_to_count_responses(Players),
    T1 = unixmtime(),
    {(T1 - T0)/1000, Result}.

repeat(Fun, N)->
    [ Fun() || _ <- lists:seq(1, N) ].

launch_balls(Players)->
    %% io:fwrite ("Throw a ball to ~w players~n", [length(Players)]),
    [ Player ! {self(), random_shot()} || Player <- Players].

start_to_count_responses(Players) ->
    count_responses(Players, dict:new()).

count_responses([], Dict)->
    dict:to_list(Dict);

count_responses(Players, Acc)->
    receive Msg ->
	    case Msg of
		"i'm dead" ->
		    count_responses(tl(Players), Acc);
		Nice_shot ->
		    New_acc1 =
			dict:update_counter("nb_of_messages", 2, Acc),
		    New_acc2 =
			dict:update_counter(Nice_shot, 1, New_acc1),
		    count_responses(Players, New_acc2)
	    end
    after 500 ->
	    exit(crashhh)
    end.


shoot_them_all(Players)->
    lists:foreach(fun(Player) ->
			  Player ! {self(), "chicken"}
		  end,
		  Players).

unixmtime () ->
    {MS, S, US} = os: timestamp (),
    (MS * 1000000 + S) * 1000 + US div 1000.

